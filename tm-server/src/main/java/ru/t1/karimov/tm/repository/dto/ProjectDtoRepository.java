package ru.t1.karimov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.karimov.tm.dto.model.ProjectDto;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public ProjectDto create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDto create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        return add(userId, project);
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final String jpql = "SELECT count(p) FROM ProjectDto p";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll() throws Exception {
        @NotNull final String jpql = "SELECT p FROM ProjectDto p";
        @NotNull final TypedQuery<ProjectDto> query = entityManager.createQuery(jpql, ProjectDto.class);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull final Comparator<ProjectDto> comparator) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT p FROM ProjectDto p ORDER BY p." + sort;
        @NotNull final TypedQuery<ProjectDto> query = entityManager.createQuery(jpql, ProjectDto.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(ProjectDto.class, id);
    }

    @Nullable
    @Override
    public ProjectDto findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT p FROM ProjectDto p";
        @NotNull final TypedQuery<ProjectDto> query = entityManager.createQuery(jpql, ProjectDto.class)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT p FROM ProjectDto p WHERE p.userId = :userId";
        @NotNull final TypedQuery<ProjectDto> query = entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull final String userId, @NotNull final Comparator<ProjectDto> comparator
    ) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT p FROM ProjectDto p WHERE p.userId = :userId ORDER BY p." + sort;
        @NotNull final TypedQuery<ProjectDto> query = entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT p FROM ProjectDto p WHERE p.id = :id AND p.userId = :userId";
        @NotNull final TypedQuery<ProjectDto> query = entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public ProjectDto findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT p FROM ProjectDto p WHERE p.userId = :userId";
        @NotNull final TypedQuery<ProjectDto> query = entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @Override
    public Long getSize(@NotNull final String userId) throws Exception {
        @NotNull String jpql = "SELECT COUNT(p) FROM ProjectDto p WHERE p.userId = :userId";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId);
        return query.getSingleResult();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String jpql = "DELETE FROM ProjectDto p";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "DELETE FROM ProjectDto p WHERE p.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
