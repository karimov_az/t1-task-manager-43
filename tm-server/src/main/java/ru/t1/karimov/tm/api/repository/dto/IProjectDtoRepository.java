package ru.t1.karimov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.model.ProjectDto;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

    @NotNull
    ProjectDto create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    ProjectDto create(@NotNull String userId, @NotNull String name) throws Exception;

}
