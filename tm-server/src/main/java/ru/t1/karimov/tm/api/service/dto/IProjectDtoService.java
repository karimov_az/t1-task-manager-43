package ru.t1.karimov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.enumerated.Status;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {

    @NotNull
    ProjectDto changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    ProjectDto changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    ProjectDto create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    ProjectDto create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    ProjectDto updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    ProjectDto updateProjectByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
