package ru.t1.karimov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.dto.IDtoRepository;
import ru.t1.karimov.tm.comparator.CreatedComparator;
import ru.t1.karimov.tm.comparator.StatusComparator;
import ru.t1.karimov.tm.dto.model.AbstractDtoModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;

public abstract class AbstractDtoRepository<M extends AbstractDtoModel> implements IDtoRepository<M> {

    @NotNull
    private static final String COLUMN_CREATED = "created";

    @NotNull
    private static final String COLUMN_STATUS = "status";

    @NotNull
    private static final String COLUMN_NAME = "name";

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator<?> comparator) {
        if (comparator == CreatedComparator.INSTANCE) return COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return COLUMN_STATUS;
        else return COLUMN_NAME;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Override
    public void removeOne(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void removeOneById(@NotNull final String id) throws Exception {
        @Nullable final M model = findOneById(id);
        if (model == null) return;
        removeOne(model);
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return;
        removeOne(model);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        removeAll();
        return add(models);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
