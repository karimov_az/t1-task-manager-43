package ru.t1.karimov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.karimov.tm.dto.model.TaskDto;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;

public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public TaskDto create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDto create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        return add(userId, task);
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final String jpql = "SELECT count(t) FROM TaskDto t";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll() throws Exception {
        @NotNull final String jpql = "SELECT t FROM TaskDto t";
        @NotNull final TypedQuery<TaskDto> query = entityManager.createQuery(jpql, TaskDto.class);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull final Comparator<TaskDto> comparator) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT t FROM TaskDto t ORDER BY t." + sort;
        @NotNull final TypedQuery<TaskDto> query = entityManager.createQuery(jpql, TaskDto.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(TaskDto.class, id);
    }

    @Nullable
    @Override
    public TaskDto findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT t FROM TaskDto t";
        @NotNull final TypedQuery<TaskDto> query = entityManager.createQuery(jpql, TaskDto.class)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT t FROM TaskDto t WHERE t.userId = :userId";
        @NotNull final TypedQuery<TaskDto> query = entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull final String userId, @NotNull final Comparator<TaskDto> comparator
    ) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT t FROM TaskDto t WHERE t.userId = :userId ORDER BY t." + sort;
        @NotNull final TypedQuery<TaskDto> query = entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT t FROM TaskDto t WHERE t.id = :id AND t.userId = :userId";
        @NotNull final TypedQuery<TaskDto> query = entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public TaskDto findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT t FROM TaskDto t WHERE t.userId = :userId";
        @NotNull final TypedQuery<TaskDto> query = entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setFirstResult(index);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull String jpql = "SELECT t FROM TaskDto t WHERE t.userId = :userId AND t.projectId = :projectId";
        @NotNull final TypedQuery<TaskDto> query = entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId);
        return query.getResultList();
    }

    @Override
    public Long getSize(@NotNull final String userId) throws Exception {
        @NotNull String jpql = "SELECT COUNT(t) FROM TaskDto t WHERE t.userId = :userId";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId);
        return query.getSingleResult();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String jpql = "DELETE FROM TaskDto t";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "DELETE FROM TaskDto t WHERE t.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
